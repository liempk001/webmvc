﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    public class PhonesController : Controller
    {
        //private QLDTEntities db = new QLDTEntities();

        // GET: Phones
        public ActionResult Index()
        {
            IEnumerable<Phone> phones = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:50861/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Phones");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Phone>>();
                    readTask.Wait();

                    phones = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    phones = Enumerable.Empty<Phone>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(phones);
        }

        // GET: Phones/Details/5
        public ActionResult Details(int? id)
        {
            Phone phone = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:50861/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Phones?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Phone>();
                    readTask.Wait();

                    phone = readTask.Result;
                }
            }

            return View(phone);
        }

        // GET: Phones/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Phones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Model,Price,GeneralNote")] Phone phone)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:50861/api/Phones");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Phone>("Phones", phone);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(phone);
        }

        // GET: Phones/Edit/5
        public ActionResult Edit(int id)
        {
            Phone phone = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:50861/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Phones?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Phone>();
                    readTask.Wait();

                    phone = readTask.Result;
                }
                
            }

            return View(phone);
        }

        // POST: Phones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Model,Price,GeneralNote")] Phone phone)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:50861/api/Phones");

                //HTTP POST
                var putTask = client.PutAsJsonAsync<Phone>("Phones", phone);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
                
            }
            return View(phone);
        }

        // GET: Phones/Delete/5
        public ActionResult Delete(int? id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:50861/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Phones/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }

        // POST: Phones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Phone phone = db.Phones.Find(id);
        //    db.Phones.Remove(phone);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
